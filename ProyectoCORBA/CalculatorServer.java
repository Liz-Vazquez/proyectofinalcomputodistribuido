import CalculatorApp.*;
import org.omg.CosNaming.*;
import org.omg.CosNaming.NamingContextPackage.*;
import org.omg.CORBA.*;
import org.omg.PortableServer.*;
import org.omg.PortableServer.POA;
import java.util.Properties;
import java.util.StringTokenizer;

class CalculatorImpl extends CalculatorPOA {
    private ORB orb;

    public void setORB(ORB orb_val){
        orb = orb_val;
    }
    //Variables
    private float resultado;
    private String respuesta, resul;
    //Implementar métodos
    public String regresaOperacion(String op) {
	    System.out.println("Se recibe la operacion: "+ op +"  en el servidor!!");
		respuesta = "Resultado de la operacion: "+ VerificarOperacion(op);
		return respuesta;
	}

    public String suma(float n1, float n2){
		resultado = n1 + n2;
		resul= String.format("%.2f", resultado);
		return resul;
	}
	public String resta(float n1, float n2){
		resultado = n1 - n2;
		resul = String.format("%.2f", resultado);
		return resul;
	}
	public String multiplicacion(float n1, float n2){
		resultado = n1 * n2;
		resul= String.format("%.2f", resultado);
		return resul;
	}
	public String division(float n1, float n2){
		if(n2==0){
			resul="Error... Division entre cero";
		}else{
			resultado = n1 / n2;
			resul= String.format("%.2f", resultado);
		}
		return resul;
	}

	public String potencia(float n1, float n2){
		resultado = (float)Math.pow(n1, n2);
		resul= String.format("%.2f", resultado);
		return resul;
	}
    public String VerificarOperacion(String texto){
        float primerNum = 0, segundoNum = 0;
        String operaciones = "", text = "";
		int count = 1;
		StringTokenizer strToken = new StringTokenizer(texto);
	     while (strToken.hasMoreTokens()) {
	    	 String token = strToken.nextToken();
	    	 if (count == 1){
	    		 primerNum 		= Float.parseFloat(token);
	    	 }else if(count == 2){
	    		 operaciones 	= token;
	    	 }else if (count == 3){
	    		 segundoNum 	= Float.parseFloat(token);
	    	 }
	    	 count ++;
	     }
		if(operaciones.equals("+")){
			text = suma(primerNum, segundoNum);
		}
		else if (operaciones.equals("-")){
			text = resta(primerNum, segundoNum);
		}
		else if(operaciones.equals("*")){
			text = multiplicacion(primerNum, segundoNum);
		}
		else if(operaciones.equals("/")){
			text = division(primerNum, segundoNum);
		}
		else if(operaciones.equals("^")){
			text = potencia(primerNum, segundoNum);
		}
		return text;
	}

    // implement shutdown() method

    public void shutdown() {
        orb.shutdown(false);
    }
}


public class CalculatorServer {

    public static void main(String args[]) {

        try{
            ORB orb = ORB.init(args, null);
            POA rootpoa = POAHelper.narrow(orb.resolve_initial_references("RootPOA"));
            rootpoa.the_POAManager().activate();
            CalculatorImpl calculatorImpl = new CalculatorImpl();
            calculatorImpl.setORB(orb);
            org.omg.CORBA.Object ref = rootpoa.servant_to_reference(calculatorImpl);
            Calculator href = CalculatorHelper.narrow(ref);
            org.omg.CORBA.Object objRef = orb.resolve_initial_references("NameService");
            NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);

            String name = "Operacion";
            NameComponent path[] = ncRef.to_name( name );
            ncRef.rebind(path, href);
            System.out.println("CalculatorServer ready and waiting ...");
            orb.run();

        }catch(Exception e){
            System.err.println("ERROR: " + e);
            e.printStackTrace(System.out);
        }
        System.out.println("CalculatorServer Exiting ...");
    }

}
