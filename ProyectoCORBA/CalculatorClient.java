import java.lang.*;
import java.lang.System;
import java.util.*;
import CalculatorApp.*;
import org.omg.CosNaming.*;
import org.omg.CORBA.*;
import org.omg.CosNaming.NamingContextPackage.*;

public class CalculatorClient {
    static Calculator calculatorImpl;

    public static void main(String args[]) {

        try{
            ORB orb = ORB.init(args, null);
            org.omg.CORBA.Object objRef = orb.resolve_initial_references("NameService");
            NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);
            String name = "Operacion";
            calculatorImpl = CalculatorHelper.narrow(ncRef.resolve_str(name));
            System.out.println("Obtained a handle on server object: " + calculatorImpl);
            boolean flag = false;
            do {
                System.out.println("\n\n**********Bienvenido(a)**********");
                System.out.println("\n1. Realizar una operacion.");
                System.out.println("\n2. Salir.");
                Scanner sc = new Scanner(System.in);
                System.out.print("\n\nIntroduzca su opcion: ");
                String opcion = sc.nextLine();
                if(opcion.equals("1")){
                    Scanner op = new Scanner(System.in);
                    System.out.println("\nEscribe una operacion: (Ejemplo: 2 + 2)");
                    System.out.println("Operaciones validas: +,-,*,/,^");
                    System.out.print("Operacion: ");
                    String operacion = op.nextLine();
                    System.out.println(calculatorImpl.regresaOperacion(operacion));
                }else if(opcion.equals("2")){
                    System.out.println("\nSaliendo...");
                    //calculatorImpl.shutdown();
                    flag=true;
                } else {
                    System.out.println("\nSeleccione una opcion correcta");
                }
            }while(!flag);

        }catch(Exception e) {
            System.out.println("ERROR : " + e);
            e.printStackTrace(System.out);
        }
    }
}
