package CalculatorApp;


/**
* CalculatorApp/CalculatorPOA.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from Calculator.idl
* viernes 5 de junio de 2020 05:18:27 PM CDT
*/

public abstract class CalculatorPOA extends org.omg.PortableServer.Servant
 implements CalculatorApp.CalculatorOperations, org.omg.CORBA.portable.InvokeHandler
{

  // Constructors

  private static java.util.Hashtable _methods = new java.util.Hashtable ();
  static
  {
    _methods.put ("regresaOperacion", new java.lang.Integer (0));
    _methods.put ("VerificarOperacion", new java.lang.Integer (1));
    _methods.put ("potencia", new java.lang.Integer (2));
    _methods.put ("suma", new java.lang.Integer (3));
    _methods.put ("resta", new java.lang.Integer (4));
    _methods.put ("multiplicacion", new java.lang.Integer (5));
    _methods.put ("division", new java.lang.Integer (6));
    _methods.put ("shutdown", new java.lang.Integer (7));
  }

  public org.omg.CORBA.portable.OutputStream _invoke (String $method,
                                org.omg.CORBA.portable.InputStream in,
                                org.omg.CORBA.portable.ResponseHandler $rh)
  {
    org.omg.CORBA.portable.OutputStream out = null;
    java.lang.Integer __method = (java.lang.Integer)_methods.get ($method);
    if (__method == null)
      throw new org.omg.CORBA.BAD_OPERATION (0, org.omg.CORBA.CompletionStatus.COMPLETED_MAYBE);

    switch (__method.intValue ())
    {
       case 0:  // CalculatorApp/Calculator/regresaOperacion
       {
         String op = in.read_string ();
         String $result = null;
         $result = this.regresaOperacion (op);
         out = $rh.createReply();
         out.write_string ($result);
         break;
       }

       case 1:  // CalculatorApp/Calculator/VerificarOperacion
       {
         String texto = in.read_string ();
         String $result = null;
         $result = this.VerificarOperacion (texto);
         out = $rh.createReply();
         out.write_string ($result);
         break;
       }

       case 2:  // CalculatorApp/Calculator/potencia
       {
         float n1 = in.read_float ();
         float n2 = in.read_float ();
         String $result = null;
         $result = this.potencia (n1, n2);
         out = $rh.createReply();
         out.write_string ($result);
         break;
       }

       case 3:  // CalculatorApp/Calculator/suma
       {
         float n1 = in.read_float ();
         float n2 = in.read_float ();
         String $result = null;
         $result = this.suma (n1, n2);
         out = $rh.createReply();
         out.write_string ($result);
         break;
       }

       case 4:  // CalculatorApp/Calculator/resta
       {
         float n1 = in.read_float ();
         float n2 = in.read_float ();
         String $result = null;
         $result = this.resta (n1, n2);
         out = $rh.createReply();
         out.write_string ($result);
         break;
       }

       case 5:  // CalculatorApp/Calculator/multiplicacion
       {
         float n1 = in.read_float ();
         float n2 = in.read_float ();
         String $result = null;
         $result = this.multiplicacion (n1, n2);
         out = $rh.createReply();
         out.write_string ($result);
         break;
       }

       case 6:  // CalculatorApp/Calculator/division
       {
         float n1 = in.read_float ();
         float n2 = in.read_float ();
         String $result = null;
         $result = this.division (n1, n2);
         out = $rh.createReply();
         out.write_string ($result);
         break;
       }

       case 7:  // CalculatorApp/Calculator/shutdown
       {
         this.shutdown ();
         out = $rh.createReply();
         break;
       }

       default:
         throw new org.omg.CORBA.BAD_OPERATION (0, org.omg.CORBA.CompletionStatus.COMPLETED_MAYBE);
    }

    return out;
  } // _invoke

  // Type-specific CORBA::Object operations
  private static String[] __ids = {
    "IDL:CalculatorApp/Calculator:1.0"};

  public String[] _all_interfaces (org.omg.PortableServer.POA poa, byte[] objectId)
  {
    return (String[])__ids.clone ();
  }

  public Calculator _this() 
  {
    return CalculatorHelper.narrow(
    super._this_object());
  }

  public Calculator _this(org.omg.CORBA.ORB orb) 
  {
    return CalculatorHelper.narrow(
    super._this_object(orb));
  }


} // class CalculatorPOA
